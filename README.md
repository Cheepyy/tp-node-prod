# NodeJS - INGESUP - B3 - Production

## Prerequisites
- [Node][1] 10
- [NPM][2] 6

## Installation

```
npm install
```

## Usage

### Run
```
npm run start
```

### Debug
```
npm run start:debug
```

### Run dist
```
npm run start:dist
```

### Lint
```
npm run lint
```

### Build
```
npm run build
```

### Endpoints

#### Books

##### Find all
```
curl -X GET http://localhost:3000/books
```

##### Find
```
curl -X GET http://localhost:3000/books/1
```

##### Create
```
curl -X POST http://localhost:3000/books \
  -H 'Content-Type: application/json' \
  -d '{
    "title": "Harry Potter et le Prince de sang-mêlé",
    "description": "Tome 6",
    "publicationDate": "2005-01-01T01:00:00+01:00",
    "authors": [
        "J. K. Rowling"
    ]
}'
```

##### Update
```
curl -X PUT http://localhost:3000/books/7 \
  -H 'Content-Type: application/json' \
  -d '{
	"id": 7,
    "title": "Harry Potter et les Reliques de la Mort",
    "description": "Tome 7",
    "publicationDate": "2007-01-01T01:00:00+01:00",
    "authors": [
        "J. K. Rowling"
    ]
}'
```

##### Delete
```
curl -X DELETE http://localhost:3000/books/8
```

## Deploy
Via [Heroku][13]

## Major dependencies
- [Express][3]
- [Nodemon][4]
- [ESLint][5]
- [Webpack][6]
- [Moment][7]
- [Compression][8]
- [Winston][9]
- [Express Winston][10]
- [PM2][11]
- [Autocannon][12]

## Author
Alexandre Escudero

[1]: https://nodejs.org/en/download/
[2]: https://www.npmjs.com/get-npm
[3]: http://expressjs.com/en/guide/routing.html
[4]: https://github.com/remy/nodemon
[5]: https://eslint.org/docs/user-guide/command-line-interface
[6]: https://webpack.js.org/concepts
[7]: http://momentjs.com
[8]: https://github.com/expressjs/compression
[9]: https://github.com/winstonjs/winston
[10]: https://github.com/bithavoc/express-winston
[11]: https://pm2.io/doc/en/runtime/overview/
[12]: https://github.com/mcollina/autocannon
[13]: https://devcenter.heroku.com/articles/deploying-nodejs
